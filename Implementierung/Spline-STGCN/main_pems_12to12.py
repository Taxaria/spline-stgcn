# STGCN Implementierung: https://github.com/FelixOpolka/STGCN-PyTorch
# PEMSD7(M) Datensatz: https://github.com/VeritasYin/STGCN_IJCAI-18
import os
import argparse
import time
import pickle as pk
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from os.path import join as pjoin

from model_pems_test import SplineSTGCN
from utils_pems_12to12 import *

use_gpu = False
epochs = 150
batch_size = 50
n = 228
n_his = 12
n_pred = 12
num_of_vertices = 228
adj_path = 'data\\pems-m\\PeMSD7_W_228.csv'
time_series_path = 'data\\pems-m\\PeMSD7_V_228.csv'

parser = argparse.ArgumentParser(description='STGCN')
parser.add_argument('--enable-cuda', action='store_true',
                    help='Enable CUDA')
args = parser.parse_args()
args.device = 'cuda'#None
if args.enable_cuda and torch.cuda.is_available():
    args.device = torch.device('cuda')
else:
    args.device = torch.device('cpu')


def train_epoch(training_input, training_target, batch_size):
    """
    Eine Trainingsepoche
    :param training_input: Eingabedaten fürs Training
    :param training_target: Zieldaten fürs Training (
    :param batch_size: Batchgröße fürs Training
    :return: Durchschnittlicher Fehler für diese Trainingsepoche
    """
    permutation = torch.randperm(int(len(training_input)))
    epoch_training_losses = []
    for i in range(0, int(len(training_input)), batch_size): 
        if i <= 7450:
            st = time.time()
            net.train()
            optimizer.zero_grad()

            indices = permutation[i:i+batch_size]
            X_batch, y_batch = make_batch_from_indices(indices, training_input, training_target, batch_size)
            X_batch = X_batch.to(device=args.device)
            y_batch = y_batch.to(device=args.device)
            out = net(adj, X_batch, edge_index, edge_attr)
            loss = loss_criterion(out, y_batch.x)
            loss.backward()
            optimizer.step()
            epoch_training_losses.append(loss.detach().cpu().numpy())
            et = time.time()
            print(f'Time for Iteration: {et-st}')
            print('i: {}'.format(i))
            print('Länge des Inputs: {}'.format(len(training_input)))
    return sum(epoch_training_losses)/len(epoch_training_losses)


if __name__ == '__main__':
    torch.manual_seed(7)
    adj = weight_matrix(adj_path)

    # Anzahl der Kanten zählen
    vals = ret_values(adj)

    # 60% train, 20% val, 20% test - Datensatz laden
    PeMS_dataset = data_gen(time_series_path, n_his + n_pred) 
    mean = PeMS_dataset.mean
    std = PeMS_dataset.std
    print('>> Datensatz geladen mit Mittelwert: {0:.2f}, Standardwabweichung: {1:.2f}'.format(mean, std))

    train = PeMS_dataset['train'].transpose(0,3,1,2)
    val = PeMS_dataset['val'].transpose(0,3,1,2)
    test = PeMS_dataset['test'].transpose(0,3,1,2)

    training_input = train[:, :, : n_his, :]
    training_input = training_input.transpose(0,3,2,1)
    training_target = train[:,:, n_his:, :]
    training_target = training_target.squeeze(1)
    training_target = training_target.transpose(0,2,1)

    val_input = val[:, :, : n_his, :]
    val_input = val_input.transpose(0,3,2,1)
    val_target = val[:, :, n_his:, :]
    val_target = val_target.squeeze(1)
    val_target = val_target.transpose(0,2,1)

    test_input = test[:, :, : n_his, :]
    test_input = test_input.transpose(0,3,2,1)
    test_target = test[:, :, n_his:, :]
    test_target = test_target.squeeze(1)
    test_target = test_target.transpose(0,2,1)

    adj = torch.from_numpy(adj)
    adj = adj.cuda()

    # Adjazenzmatrix für die Spline-CNNs in COO-Format umwandeln
    edge_index = fromA2edgeindex(adj)
    # Aus der Adjazenzmatrix die Kantenattribute bestimmen
    edge_attr = fromA2edge_attr(adj)

    edge_index = edge_index.cuda()
    edge_attr = edge_attr.cuda()

    net = SplineSTGCN(adj.shape[0],
                training_input.shape[3],
                n_his,
                n_pred).to(device=args.device)

    optimizer = torch.optim.Adam(net.parameters(), lr=1e-5)
    loss_criterion = nn.MSELoss()
    # Aus den Trainingsdaten Data-Objekte erzeugen:
    # training_input_datalist, training_target_datalist = make_training_datalists(training_input, training_target, edge_index, edge_attr)
    # val_input_datalist, val_target_datalist = make_val_datalists(val_input, val_target, edge_index, edge_attr)
    # Listen mit Data-Objekten laden:
    training_input_datalist = list(pickle.load(open('data_new\\pems-m\\training_input_datalist_12to12', 'rb')))
    training_target_datalist = list(pickle.load(open('data_new\\pems-m\\training_target_datalist_12to12', 'rb')))
    val_input_datalist =  list(pickle.load(open('data_new\\pems-m\\val_input_datalist_12to12', 'rb')))
    val_target_datalist =  list(pickle.load(open('data_new\\pems-m\\val_target_datalist_12to12', 'rb')))
 

    training_losses = []
    validation_losses = []
    validation_maes = []
    validation_rmses = []
    validation_mapes = []
    print('Start Training')
    for epoch in range(epochs):
        est = time.time()
        loss = train_epoch(training_input_datalist, training_target_datalist,
                           batch_size=batch_size)
        training_losses.append(loss)

        # Run validation
        with torch.no_grad():
            net.eval()

            permutation = torch.randperm(int(len(val_input)))
            for i in range (0, int(len(val_input_datalist)), batch_size):
                if i < 2500:
                    indices = permutation[i:i+batch_size]

                    X_batch, y_batch = make_batch_from_indices(indices, val_input_datalist, val_target_datalist, batch_size)
                    X_batch = X_batch.to(device=args.device)
                    y_batch = y_batch.to(device=args.device)
            
                out = net(adj, X_batch, edge_index, edge_attr)

                val_loss = loss_criterion(out, y_batch.x).to(device="cpu")

            validation_losses.append(np.asscalar(val_loss.detach().numpy()))
            out_unnormalized = out.detach().cpu().numpy()*std+mean
            target_unnormalized = y_batch.x.detach().cpu().numpy()*std+mean
            mae = mae_error(out_unnormalized, target_unnormalized)
            rmse = rmse_error(out_unnormalized, target_unnormalized)
            mape = mape_error(out_unnormalized, target_unnormalized,)
            validation_maes.append(mae)
            validation_rmses.append(rmse)
            validation_mapes.append(mape)
            out = None
            X_batch = X_batch.to(device='cuda')
            y_batch = y_batch.to(device='cuda')
        eet = time.time()    
        print('Epoche: {}'.format(epoch))
        print("Trainingsfehler: {}".format(training_losses[-1]))
        print("Validierungsfehler: {}".format(validation_losses[-1]))
        print("MAE: {}".format(validation_maes[-1]))
        print("RMSE: {}".format(validation_rmses[-1]))
        print("MAPE: {}".format(validation_mapes[-1]))
        print(f'Zeit für Epoche: {eet-est}')
        print_losses(training_losses, validation_losses, validation_maes, validation_rmses, validation_mapes)

        with open('Ausgabe\\losses_new.pk', "wb") as fd:
            pk.dump((training_losses, validation_losses, validation_maes), fd)
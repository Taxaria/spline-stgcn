# STGCN Implementierung und METR-LA Datensatz: https://github.com/FelixOpolka/STGCN-PyTorch
import os
import argparse
import time
import pickle as pk
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn

from model_metrla_12to12 import SplineSTGCN
from utils_metrla_12to12 import *

use_gpu = False
num_timesteps_input = 12 # Standard 12
num_timesteps_output = 12 # Standard 3

epochs = 100
batch_size = 50 # Standard 50

parser = argparse.ArgumentParser(description='STGCN')
parser.add_argument('--enable-cuda', action='store_true',
                    help='Enable CUDA')
args = parser.parse_args()
args.device = 'cuda'
if args.enable_cuda and torch.cuda.is_available():
    args.device = torch.device('cuda')
else:
    args.device = torch.device('cpu')


def train_epoch(training_input, training_target, batch_size):
    """
    Eine Trainingsepoche
    :param training_input: Eingabedaten fürs Training
    :param training_target: Zieldaten fürs Training
    :param batch_size: Batchgröße fürs Training
    :return: Durchschnittlicher Fehler für diese Trainingsepoche
    """

    epoch_training_losses = []
    for i in range(int(len(training_input))):
        st = time.time()
        net.train()
        optimizer.zero_grad()

        X_batch, y_batch = training_input[i], training_target[i]
        X_batch = X_batch.to(device=args.device)
        y_batch = y_batch.to(device=args.device)

        out = net(A_wave, X_batch, edge_index, edge_attr)
        loss = loss_criterion(out, y_batch.x)
        loss.backward()
        optimizer.step()
        epoch_training_losses.append(loss.detach().cpu().numpy())
        et = time.time()
        print(f'Time for Iteration: {et-st}')
        print('i: {}'.format(i))
        print('Länge des Inputs: {}'.format(len(training_input)))
    return sum(epoch_training_losses)/len(epoch_training_losses)


if __name__ == '__main__':
    torch.manual_seed(7)

    # Datensatz laden
    A, X, means, stds = load_metr_la_data()
    # Grenzen für die Größe des Trainings- und Validierungsdatensatz festelegen
    split_line1 = int(X.shape[2] * 0.6)
    split_line2 = int(X.shape[2] * 0.8)

    # Datensatz ind Training-, Validierungs- und Testdatensatz aufteilen
    train_original_data = X[:, :, :split_line1]
    val_original_data = X[:, :, split_line1:split_line2]
    test_original_data = X[:, :, split_line2:]

    # Datensätze entsprechend des betrachteten Kontext und dem zu vorhersagenden Zeitfenster aufteilen
    training_input, training_target = generate_dataset(train_original_data,
                                                       num_timesteps_input=num_timesteps_input,
                                                       num_timesteps_output=num_timesteps_output)
    val_input, val_target = generate_dataset(val_original_data,
                                             num_timesteps_input=num_timesteps_input,
                                             num_timesteps_output=num_timesteps_output)
    test_input, test_target = generate_dataset(test_original_data,
                                               num_timesteps_input=num_timesteps_input,
                                               num_timesteps_output=num_timesteps_output)
    # Normalisierte Adjazenzmatrix berechnen
    A_wave = get_normalized_adj(A)
    A_wave = torch.from_numpy(A_wave)
    A_wave = A_wave.to(device=args.device)

    # Adjazenzmatrix für die Spline-CNNs in COO-Format umwandeln
    edge_index = fromA2edgeindex(A)
    # Aus der Adjazenzmatrix die Kantenattribute bestimmen
    edge_attr = fromA2edge_attr(A_wave)

    edge_index = edge_index.cuda()
    edge_attr = edge_attr.cuda()


    # Model laden
    net = SplineSTGCN(A_wave.shape[0],
                training_input.shape[3],
                num_timesteps_input,
                num_timesteps_output).to(device=args.device)
    

    # Optimierungsfunktion mit Lernrate festlegen
    optimizer = torch.optim.Adam(net.parameters(), lr=1e-4)
    # Fehlerfunktion festlegen
    loss_criterion = nn.MSELoss()

    # Wenn der Input noch nicht als Batches für die Spline-CNNs vorliegt:
    # new_val_input = make_val_input_batches(val_input, edge_index, edge_attr, batch_size)
    # new_val_target = make_val_target_batches(val_target, edge_index, edge_attr, batch_size)
    # new_training_input = make_training_input_batches(training_input, edge_index, edge_attr, batch_size)
    # new_training_target = make_training_target_batches(training_target, edge_index, edge_attr, batch_size)
    # Batches laden:
    new_training_input = list(pickle.load(open('data\\training_input_batches_bs50_12to12', 'rb')))
    new_training_target = list(pickle.load(open('data\\training_target_batches_bs50_12to12', 'rb')))
    new_val_input = list(pickle.load(open('data\\val_input_batches_bs50_12to12', 'rb')))
    new_val_target = list(pickle.load(open('data\\val_target_batches_bs50_12to12', 'rb')))
 

    training_losses = []
    validation_losses = []
    validation_maes = []
    validation_rmses = []
    validation_mapes = []
    print('Start Training')
    # Traininsprozess starten
    for epoch in range(epochs):
        est = time.time()
        # Durchschnittlichen Fehler einer Trainingsepoche berechnen
        loss = train_epoch(new_training_input, new_training_target,
                           batch_size=batch_size)
        training_losses.append(loss)

        # Modell auf Validierungsdaten testen
        with torch.no_grad():
            net.eval()

            for i in range(int(len(new_val_input))):
                X_batch, y_batch = new_val_input[i], new_val_target[i]
                X_batch = X_batch.to(device=args.device)
                y_batch = y_batch.to(device=args.device)

                # Vorhersage des Modells für die übergebenen Informationen
                out = net(A_wave, X_batch, edge_index, edge_attr)

                # Fehler für die Vorhersage berechnen
                val_loss = loss_criterion(out, y_batch.x).to(device="cpu")

            validation_losses.append(np.asscalar(val_loss.detach().numpy()))
            out_unnormalized = out.detach().cpu().numpy()*stds[0]+means[0]
            target_unnormalized = y_batch.x.detach().cpu().numpy()*stds[0]+means[0]
            # Fehlermaße berechnen
            mae = mae_error(out_unnormalized, target_unnormalized)
            rmse = rmse_error(out_unnormalized, target_unnormalized)
            mape = mape_error(out_unnormalized, target_unnormalized,)
            validation_maes.append(mae)
            validation_rmses.append(rmse)
            validation_mapes.append(mape)
            out = None
            X_batch = X_batch.to(device='cuda')#to(device="cpu")
            y_batch = y_batch.to(device='cuda')#to(device="cpu")
        eet = time.time()    
        # Fehler nach jeder Epoche ausgeben
        print('Epoche: {}'.format(epoch))
        print("Trainingsfehler: {}".format(training_losses[-1]))
        print("Validierungsfehler: {}".format(validation_losses[-1]))
        print("MAE: {}".format(validation_maes[-1]))
        print("RMSE: {}".format(validation_rmses[-1]))
        print("MAPE: {}".format(validation_mapes[-1]))
        print(f'Zeit für Epoche: {eet-est}')
        print_losses(training_losses, validation_losses, validation_maes, validation_rmses, validation_mapes)

        with open('Ausgabe\\losses_new.pk', "wb") as fd:
            pk.dump((training_losses, validation_losses, validation_maes), fd)
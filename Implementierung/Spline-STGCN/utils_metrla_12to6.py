import os
import zipfile
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch_geometric.data import Data
from torch_geometric.data import Batch
import pickle
import pandas as pd

batch_size = 50
last_A = None
last_edge_index = None
last_edge_attr = None

def load_metr_la_data():
    '''
    Lädt den Datensatz 
    Gibt Adjazenzmatrix, Knotenmerkmale, Mittelwert und Standardabweichung zurück 
    '''
    if (not os.path.isfile('data\\adj_mat.npy')
        or not os.path.isfile('data\\node_values.npy')):
        with zipfile.ZipFile('data\\METR-LA.zip', 'r') as zip_ref:
            zip_ref.extractall('data\\')
    A = np.load('data\\adj_mat.npy')
    X = np.load('data\\node_values.npy').transpose((1,2,0))
    X = X.astype(np.float32)
    # Z-Score Normalisierung
    means = np.mean(X, axis=(0,2))
    X = X - means.reshape(1, -1, 1)
    stds = np. std(X, axis=(0,2))
    X = X / stds.reshape(1,-1,1)
    return A, X, means, stds

def get_normalized_adj(A):
    """
    Gibt die normalisierte Adjazenzmatrixz zurück - 1st order Approximation
    """
    A = A + np.diag(np.ones(A.shape[0], dtype=np.float32))
    D = np.array(np.sum(A, axis =1)).reshape((-1,))
    D[D <= 10e-5] = 10e-5 #Prevent infs
    diag = np.reciprocal(np.sqrt(D))
    A_wave = np. multiply(np.multiply(diag.reshape((-1 ,1)), A),
                          diag.reshape((1,-1)))
    return A_wave

def generate_dataset(X, num_timesteps_input, num_timesteps_output):
    """
    Bekommt die Knotenmerkmale des Graphen übergeben und teilt diese in mehrere Beispiele
    entlang der Zeitachse, entsprechend dem übergebenen Zeitfenster.
    :param X: Knotenmerkmale
    :return: Knotenmerkmale entsprechend der Knotenxtgröße und Knotenmerkmale entsprechend
    dem zu vorhersagenden Zeitfenster.
    """
    # Erzeugt den Anfangs- und Endindex eines Beispiels
    indices = [(i, i + (num_timesteps_input + num_timesteps_output)) for i
               in range(X.shape[2] - (
                num_timesteps_input + num_timesteps_output) + 1)]

    features, target = [], []
    for i, j in indices:
        features.append(
            X[:, :, i: i + num_timesteps_input].transpose(
                (0, 2, 1)))
        target.append(X[:, 0, i + num_timesteps_input: j])

    return torch.from_numpy(np.array(features)), \
           torch.from_numpy(np.array(target))

def fromA2edgeindex(A):
    '''
    Erzeugt aus der Adjazenzmatrix die Kantenrepräsentation in COO-Format
    :param A: Adjazenzmatrix
    :return: Adjazenzmatrix in COO-Format
    '''
    global last_A
    global last_edge_index
    if type(last_A) != type(None) and type(last_A) == type(A) and type(last_edge_index) != type(None):
        return last_edge_index
    edge_index = torch.IntTensor()
    ones = count_ones(A)
    edge_index = edge_index.new_empty(2, ones)
    colcounter = 0
    for i in range (A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] > 0:
                edge_index[0][colcounter] = i
                edge_index[1][colcounter] = j
                colcounter += 1
    last_A = A
    last_edge_index = edge_index
    return edge_index

def fromA2edge_attr(A_wave):
    '''
    Erzeugt aus der normalisierten Adjazenzmatrix die Kantengewichte
    :param A_wave: normalisiserte Adjazenzmatrix
    :return: Adjazenzmatrix in COO-Format
    '''
    edge_attr = torch.Tensor()
    ones = count_ones(A_wave)
    edge_attr = edge_attr.new_empty(ones, 2)
    ccounter = 0
    for i in range(A_wave.shape[0]):
        for j in range(A_wave.shape[1]):
            if (A_wave[i][j] > 0):
                edge_attr[ccounter][0] = A_wave[i][j] 
                edge_attr[ccounter][1] = A_wave[j][i]
                ccounter += 1
    last_edge_attr = edge_attr
    return edge_attr

def make_x_batch(X, A, edge_index, pseudo):
    '''
    Erzeugt aus den Knotenmerkmalen einen für die Spline-CNNs geeigneten Batch
    :param X: Knotenmerkmale
    :param A: Adjzanzematrix
    :param edge_index: Adjazenzmatrix im COO-Format
    :param pseudo: Pseudokoordinaten bzw. Kantengewichte
    :return: Ein Batch-Objekt
    '''
    dlist = []
    x = X
    if x.shape[0] != batch_size: 
        x = x.reshape(batch_size, int(x.shape[0]/batch_size), x.shape[1])
    edge_index = edge_index
    pseudo = pseudo
    
    for batch in range(x.shape[0]):
        data=Data(x=x.select(0,batch), edge_index = edge_index, edge_attr = pseudo)
        dlist.append(data)
    
    x_batch = Batch.from_data_list(dlist)
    return x_batch

def count_ones(A):
    '''
    Zählt die Anzahl der vorhandenen Kanten
    :param A: Adjazenzmatrix
    :return: Anzahl der Kanten des Graphen  
    '''
    ones = 0
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] > 0:
                ones += 1
    return ones

def make_training_input_batches(training_input, edge_index, edge_attr, batch_size):
    '''
    Erzeugt aus den Traingsdaten entsprechend für die Spline-CNNs geeignete Batches.
    :param training_input: Trainingseingabedaten
    :param edge_index: Adjazenzmatrix im COO-Format
    :param edge_attr: Kantenattribute
    :param batch_size: Batchgröße
    :return: Geeigneten Batch der Trainingsdaten für die Spline-CNNs
    '''
    datalist = []
    batchlist = []
    batch_counter = int(training_input.shape[0] / batch_size) # Anzahl der möglichen Batches
    x = torch.Tensor()
    x = x.new_empty(training_input.shape[1], training_input.shape[2], training_input.shape[3])
    training_data_counter = 0
    for i in range (batch_counter):
        for counter in range(batch_size):
            for sensors in range(training_input.shape[1]):
                for timesteps in range(training_input.shape[2]):
                    x[sensors][timesteps][0] = training_input[training_data_counter][sensors][timesteps][0] 
                    x[sensors][timesteps][1] = training_input[training_data_counter][sensors][timesteps][1]
            training_data_counter +=1
            data = Data(x = x, edge_index = edge_index, edge_attr = edge_attr)
            datalist.append(data)
        training_batch = Batch.from_data_list(datalist)
        batchlist.append(training_batch)
        datalist = []
        print(i)
    pickle.dump(batchlist, open('data\\training_input_batches_bs50_12to6', 'wb'))
    return batchlist

def make_training_target_batches(training_target, edge_index, edge_attr, batch_size):
    '''
    Erzeugt aus den Ziel-Traingsdaten entsprechend für die Spline-CNNs geeignete Batches.
    :param training_target: Ziel-Trainingseingabedaten
    :param edge_index: Adjazenzmatrix im COO-Format
    :param edge_attr: Kantenattribute
    :param batch_size: Batchgröße
    :return: Geeigneten Batch der Ziel-Trainingsdaten für die Spline-CNNs
    '''
    datalist = []
    batchlist = []
    x = torch.Tensor()
    x = x.new_empty(training_target.shape[1], training_target.shape[2])
    training_data_counter = 0
    batch_counter = int(training_target.shape[0] / batch_size)
    for i in range(batch_counter):
        for counter in range(batch_size):
            for sensor in range(training_target.shape[1]):
                for timesteps in range(training_target.shape[2]):
                    x[sensor][timesteps] = training_target[training_data_counter][sensor][timesteps]
            training_data_counter += 1
            data = Data(x = x, edge_index = edge_index, edge_attr = edge_attr)
            datalist.append(data)
        training_batch = Batch.from_data_list(datalist)
        batchlist.append(training_batch)
        datalist = []
        print(i)
    pickle.dump(batchlist, open('data\\training_target_batches_bs50_12to6', 'wb'))
    return batchlist

def  make_val_input_batches(val_input, edge_index, edge_attr, batch_size):
    '''
    Erzeugt aus den Validierungsdaten entsprechend für die Spline-CNNs geeignete Batches.
    :param val_input: Validierungseingabedaten
    :param edge_index: Adjazenzmatrix im COO-Format
    :param edge_attr: Kantenattribute
    :param batch_size: Batchgröße
    :return: Geeigneten Batch der Validierungsdaten für die Spline-CNNs
    '''
    datalist = []
    batchlist = []
    batch_counter = int(val_input.shape[0] / batch_size) # Anzahl der möglichen Batches
    x = torch.Tensor()
    x = x.new_empty(val_input.shape[1], val_input.shape[2], val_input.shape[3])
    training_data_counter = 0
    for i in range (batch_counter):
        for counter in range(batch_size):
            for sensors in range(val_input.shape[1]):
                for timesteps in range(val_input.shape[2]):
                    x[sensors][timesteps][0] = val_input[training_data_counter][sensors][timesteps][0] 
                    x[sensors][timesteps][1] = val_input[training_data_counter][sensors][timesteps][1]
            training_data_counter +=1
            data = Data(x = x, edge_index = edge_index, edge_attr = edge_attr)
            datalist.append(data)
        training_batch = Batch.from_data_list(datalist)
        batchlist.append(training_batch)
        datalist = []
        print(i)
    pickle.dump(batchlist, open('data\\val_input_batches_bs50_12to6', 'wb'))
    return batchlist

def make_val_target_batches(val_target, edge_index, edge_attr, batch_size):
    '''
    Erzeugt aus den Ziel-Validierungsdaten entsprechend für die Spline-CNNs geeignete Batches.
    :param val_target: Ziel-Validierungsdaten
    :param edge_index: Adjazenzmatrix im COO-Format
    :param edge_attr: Kantenattribute
    :param batch_size: Batchgröße
    :return: Geeigneten Batch der Ziel-Validierungsdaten für die Spline-CNNs
    '''
    datalist = []
    batchlist = []
    x = torch.Tensor()
    x = x.new_empty(val_target.shape[1], val_target.shape[2])
    training_data_counter = 0
    batch_counter = int(val_target.shape[0] / batch_size)
    for i in range(batch_counter):
        for counter in range(batch_size):
            for sensor in range(val_target.shape[1]):
                for timesteps in range(val_target.shape[2]):
                    x[sensor][timesteps] = val_target[training_data_counter][sensor][timesteps]
            training_data_counter += 1
            data = Data(x = x, edge_index = edge_index, edge_attr = edge_attr)
            datalist.append(data)
        training_batch = Batch.from_data_list(datalist)
        batchlist.append(training_batch)
        datalist = []
        print(i)
    pickle.dump(batchlist, open('data\\val_target_batches_bs50_12to6', 'wb'))
    return batchlist

def print_losses(training_losses, validation_losses, validation_maes, validation_rmses, validation_mapes):
    '''
    Erzeugt jeweils eine Grafik für ein Fehlermaß
    :param trainings_losses: Liste mit Trainingsfehler
    :param validation_losses: Liste mit Validierungsfehler
    :param validation_maes: Liste mit mean absolute error
    :param validation_rmses: Liste mit root mean square error
    :param validation_mapes: Liste mit mean absolute percentage error
    :return: Grafiken mit den Fehlermaßen für die aktuelle Trainingsepoche
    '''
    plt.figure(1)
    plt.plot(training_losses, label = 'Trainingsfehler')
    plt.plot(validation_losses, label = 'Validierungsfehler')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('Fehler')
    plt.savefig('Ausgabe\\training_and_val_loss_12to6.png', )
    plt.cla()
    plt.figure(2)
    plt.plot(validation_maes, label = 'MAE')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('MAE')
    plt.savefig('Ausgabe\\val_mae_loss_12to6.png', )
    plt.cla()
    plt.figure(3)
    plt.plot(validation_rmses, label = 'RMSE')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('RMSE')
    plt.savefig('Ausgabe\\val_rmse_loss_12to6.png', )
    plt.cla()
    plt.figure(4)
    plt.plot(validation_mapes, label = 'MAPE')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('MAPE')
    plt.savefig('Ausgabe\\val_mape_loss_12to6.png', )
    plt.cla()

def mae_error(out_unnormalized, target_unnormalized):
    '''
    Berechnet den mean absolute error
    :param out_unnormalized: Vorhersage des Modells
    :param target_unnormalized: Zielwert
    :return: mean absolute error
    '''
    mae = np.mean(np.absolute(out_unnormalized - target_unnormalized))
    return mae

def mape_error(out_unnormalized, target_unnormalized):
    '''
    Berechnet den mean absolute percentage error. Falls ein Wert nicht vorhanden ist,
    wird der Wert ignoriert.
    :param out_unnormalized: Vorhersage des Modells
    :param target_unnormalized: Zielwert
    :return: mean absolute percentage error
    '''
    c = 0
    out_copy = np.copy(out_unnormalized)
    target_copy = np.copy(target_unnormalized)
    for i in range (target_unnormalized.shape[0]):
        for j in range (target_unnormalized.shape[1]):
            if target_unnormalized[i][j] == 0.0:
                c += 1
                target_copy[i][j] = 1
                out_copy[i][j] = 1
    print(f'Anzahl Nullen:{c}')
    mape = 100 *(np.mean(np.absolute((target_copy - out_copy) / target_copy)))
    #mape = np.mean(np.abs(out_unnormalized-target_unnormalized) / (target_unnormalized + 1e-5)) * 100
    return mape

def rmse_error(out_unnormalized, target_unnormalized):
    '''
    Berechnet den root mean square error
    :param out_unnormalized: Vorhersage des Modells
    :param target_unnormalized: Zielwert
    :return: root mean square error
    '''
    #rmse = (np.mean((out_unnormalized-target_unnormalized)**2))**(0.5)
    rmse = np.sqrt(np.mean((out_unnormalized-target_unnormalized)**2))
    return rmse

def count_vals(adj):
    '''
    Zählt alle Werte in adj die größer als 0 sind.
    :param adj: Adjazenzmatrix
    :return: Anzahl der Werte größer 0
    ''' 
    vals = 0
    for i in range (adj.shape[0]):
        for j in range (adj.shape[1]):
            if adj[i][j] > 0.0:
                vals += 1 
    return vals

def intervalrange(X):
    '''
    Gibt den kleinsten und den größten Wert des übergebenen Arrays aus
    :param X: Array mit Knotenmerkmalen
    '''
    min = 50
    max = 0
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            for z in range(X.shape[2]):
                if (X[i][j][z] < min and j == 0):
                    min = X[i][j][z]
                if (X[i][j][z] > max and j==0):
                    max = X[i][j][z]
    print(f'Minimum:{min} und Maximum:{max}')

def impute_vals(X):
    X_hat = torch.clone(X)


    return X_hat
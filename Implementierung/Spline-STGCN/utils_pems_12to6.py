import os
import csv
import zipfile
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
from torch_geometric.data import Data
from torch_geometric.data import Batch
import pickle
from scipy.sparse.linalg import eigs

batch_size = 50
last_A = None
last_edge_index = None
last_edge_attr = None

# Klasse für den Datensatz
class Dataset(object):
    def __init__(self, data, stats):
        self.__data = data
        self.mean = stats['mean']
        self.std = stats['std']

    def __getitem__(self, key):
        return self.__data[key]

    def get_stats(self):
        return {'mean': self.mean, 'std': self.std}

    def z_inverse(self, type_):
        return self.__data[type_] * self.std + self.mean

def seq_gen(data_seq, n_frame):
    '''
    Erzzeugt Daten in der Form einer Sequenz
    :param data_seq: np.ndarray, Zeitreihe von der Form (length, num_nodes)
    :param n_frame: Zeitfenster: n_his + n_pred
    :return: Array der Form (length - n_frame + 1, n_frame, num_of_vertices, 1)
    '''

    data = np.zeros(shape=(data_seq.shape[0] - n_frame + 1,
                           n_frame, data_seq.shape[1], 1))
    for i in range(data_seq.shape[0] - n_frame + 1):
        data[i, :, :, 0] = data_seq[i: i + n_frame, :]
    return data


def data_gen(file_path, n_frame=15):
    '''
    Lädt die Quelldatei und erzeugt den Datensatz
    :param file_path: Pfad der Datei
    :param n_frame: Zeitfenster n_his + n_pred
    :return: Datensatz mit Trainings-, Validierungs- und Testdatensatz
    '''

    with open(file_path, 'r') as f:
        reader = csv.reader(f)
        data_seq = np.array([list(map(float, i)) for i in reader if i])

    num_of_samples = data_seq.shape[0]
    splitting_line1 = int(num_of_samples * 0.6)
    splitting_line2 = int(num_of_samples * 0.8)

    seq_train = seq_gen(data_seq[: splitting_line1], n_frame)
    seq_val = seq_gen(data_seq[splitting_line1: splitting_line2], n_frame)
    seq_test = seq_gen(data_seq[splitting_line2:], n_frame)

    mean = np.mean(seq_train)
    std = np.std(seq_train)
    x_stats = {'mean': mean, 'std': std}

    x_train = z_score(seq_train, mean, std)
    x_val = z_score(seq_val, mean, std)
    x_test = z_score(seq_test, mean, std)

    x_data = {'train': x_train, 'val': x_val, 'test': x_test}
    dataset = Dataset(x_data, x_stats)
    return dataset


def gen_batch(inputs, batch_size, dynamic_batch=False, shuffle=False):
    '''
    Iterator für einen Batch.
    :param inputs: np.ndarray, Datensequenz
    :param batch_size: Batchgröße
    :param dynamic_batch: Ob die Batchgröße des letzten Batches geändert werden soll,
    falls diese kleiner als die übergebene Batchgröße ist
    :param shuffle: Ob die Batches gemischt werden sollen
    '''
    len_inputs = len(inputs)

    if shuffle:
        idx = np.arange(len_inputs)
        np.random.shuffle(idx)

    for start_idx in range(0, len_inputs, batch_size):
        end_idx = start_idx + batch_size
        if end_idx > len_inputs:
            if dynamic_batch:
                end_idx = len_inputs
            else:
                break
        if shuffle:
            slide = idx[start_idx:end_idx]
        else:
            slide = slice(start_idx, end_idx)

        yield inputs[slide]

def z_score(x, mean, std):
    '''
    Z-Scote Normalisiserung
    :param x: Übergebenen Werte
    :param mean: Mittelwert
    :param std: Standardabweichung
    :return: Normalisierte Werte
    '''
    return (x - mean) / std


def z_inverse(x, mean, std):
    '''
    Inverse Funltion zu z_score()
    ----------
    :param x: Übergebene Werte
    :param mean: Mittelwert
    :param std: Standardabweichung
    :return: Renormalisierte Werte
    '''
    return x * std + mean

def scaled_laplacian(W):
    '''
    Berechnet die normalisierte Graph Laplace-Matrix
    :param W: Adjazenzmatrix
    :return: Normalisiserte Graph Laplace-Matrix
    '''

    num_of_vertices = W.shape[0]
    d = np.sum(W, axis=1)
    L = np.diag(d) - W
    for i in range(num_of_vertices):
        for j in range(num_of_vertices):
            if (d[i] > 0) and (d[j] > 0):
                L[i, j] = L[i, j] / np.sqrt(d[i] * d[j])
    # lambda_max \approx 2.0, the largest eigenvalues of L.
    lambda_max = eigs(L, k=1, which='LR')[0][0].real
    return 2 * L / lambda_max - np.identity(num_of_vertices)


def cheb_poly_approx(L, order_of_cheb):
    '''
    Chebyshev Polynom Approximation
    Parameters
    ----------
    :param L: Skalierte Graph Laplace-Matrix
    :return: Chebyshev Polynom Approximation
    '''

    if order_of_cheb == 1:
        return np.identity(L.shape[0])

    cheb_polys = [np.identity(L.shape[0]), L]

    for i in range(2, order_of_cheb):
        cheb_polys.append(2 * L * cheb_polys[i - 1] - cheb_polys[i - 2])

    return np.concatenate(cheb_polys, axis=-1)

def first_approx(adj):
    '''
    Berechnet 1st-order Approximation
    :param: adj: Adjazenzmatrix
    :return: Aproximation erster Ordnung
    '''
    A = adj + np.identity(adj.shape[0])
    sinvD = np.sqrt(np.diag(np.sum(A, axis=1)).I)
    # Bezug zu Glg.5
    return np.identity(adj.shape[0]) + sinvD * A * sinvD


def weight_matrix(file_path, sigma2=0.1, epsilon=0.5, scaling=True):
    '''
    Gewichtsmatrix laden
    ----------
    :param file_path: Pfad der Adjazenzmatrix
    :param sigma2: Sigma^2, Standard 0.1
    :param epsilon: Epsilon, Standard 0.5, Schwellwert
    :param scaling: Standard True, gibt an, ob die Matrix nummerisch
    skaliert werden soll
    :return: Gewichtsmatrix
    '''
    with open(file_path, 'r') as f:
        reader = csv.reader(f)
        W = np.array([list(map(float, i)) for i in reader if i])

    # check whether adj is a 0/1 matrix.
    if set(np.unique(W)) == {0, 1}:
        print('The input graph is a 0/1 matrix; set "scaling" to False.')
        scaling = False

    if scaling:
        n = W.shape[0]
        W = W / 10000
        W2, W_mask = W*W, np.ones([n,n]) - np.identity(n)
        # refer to Eq.10
        return np.exp(-W2 / sigma2) * (np.exp(-W2 / sigma2) >= epsilon) * W_mask
    return adj

def print_losses(training_losses, validation_losses, validation_maes, validation_rmses, validation_mapes):
    '''
    Erzeugt jeweils eine Grafik für ein Fehlermaß
    :param trainings_losses: Liste mit Trainingsfehler
    :param validation_losses: Liste mit Validierungsfehler
    :param validation_maes: Liste mit mean absolute error
    :param validation_rmses: Liste mit root mean square error
    :param validation_mapes: Liste mit mean absolute percentage error
    :return: Grafiken mit den Fehlermaßen für die aktuelle Trainingsepoche
    '''
    plt.figure(1)
    plt.plot(training_losses, label = 'Trainingsfehler')
    plt.plot(validation_losses, label = 'Validierungsfehler')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('Fehler')
    plt.savefig('Ausgabe\\training_and_val_loss_12to6.png', )
    plt.cla()
    plt.figure(2)
    plt.plot(validation_maes, label = 'MAE')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('MAE')
    plt.savefig('Ausgabe\\val_mae_loss_12to6.png', )
    plt.cla()
    plt.figure(3)
    plt.plot(validation_rmses, label = 'RMSE')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('RMSE')
    plt.savefig('Ausgabe\\val_rmse_loss_12to6.png', )
    plt.cla()
    plt.figure(4)
    plt.plot(validation_mapes, label = 'MAPE')
    plt.legend()
    plt.title('')
    plt.xlabel('Epochen')
    plt.ylabel('MAPE')
    plt.savefig('Ausgabe\\val_mape_loss_12to6.png', )
    plt.cla()

def mae_error(out_unnormalized, target_unnormalized):
    '''
    Berechnet den mean absolute error
    :param out_unnormalized: Vorhersage des Modells
    :param target_unnormalized: Zielwert
    :return: mean absolute error
    '''
    mae = np.mean(np.absolute(target_unnormalized - out_unnormalized))
    return mae

def mape_error(out_unnormalized, target_unnormalized):
    '''
    Berechnet den mean absolute percentage error. Falls ein Wert nicht vorhanden ist,
    wird der Fehler auf 50% gesetzt.
    :param out_unnormalized: Vorhersage des Modells
    :param target_unnormalized: Zielwert
    :return: mean absolute percentage error
    '''
    c = 0
    out_copy = np.copy(out_unnormalized)
    target_copy = np.copy(target_unnormalized)
    for i in range (target_unnormalized.shape[0]):
        for j in range (target_unnormalized.shape[1]):
            if target_unnormalized[i][j] == 0:
                c += 1
                target_copy[i][j] = 1
                out_copy[i][j] = 0
    print(f'Anzahl Nullen:{c}')
    mape = 100 *(np.mean(np.absolute((target_copy - out_copy) / target_copy)))
    return mape

def rmse_error(out_unnormalized, target_unnormalized):
    '''
    Berechnet den root mean square error
    :param out_unnormalized: Vorhersage des Modells
    :param target_unnormalized: Zielwert
    :return: root mean square error
    '''
    rmse = (np.mean((out_unnormalized-target_unnormalized)**2))**(0.5)
    return rmse

# Hilfsfunktionen für Graphen:
def fromA2edgeindex(A):
    '''
    Erzeugt aus der Adjazenzmatrix die Kantenrepräsentation in COO-Format
    :param A: Adjazenzmatrix
    :return: Adjazenzmatrix in COO-Format
    '''
    global last_A
    global last_edge_index
    if type(last_A) != type(None) and type(last_A) == type(A) and type(last_edge_index) != type(None):
        return last_edge_index
    edge_index = torch.IntTensor()
    values = count_values(A)
    edge_index = edge_index.new_empty(2, values)
    colcounter = 0
    for i in range (A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] != 0.0:
                edge_index[0][colcounter] = i
                edge_index[1][colcounter] = j
                colcounter += 1
    last_A = A
    last_edge_index = edge_index
    return edge_index

def fromA2edge_attr(A):
    '''
    Erzeugt aus der normalisierten Adjazenzmatrix die Kantengewichte
    :param A_wave: normalisiserte Adjazenzmatrix
    :return: Adjazenzmatrix in COO-Format
    '''
    edge_attr = torch.Tensor()
    ones = count_values(A)
    edge_attr = edge_attr.new_empty(ones, 2)
    ccounter = 0
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if (A[i][j] > 0):
                edge_attr[ccounter][0] = A[i][j] 
                edge_attr[ccounter][1] = A[j][i]
                ccounter += 1
    last_edge_attr = edge_attr
    return edge_attr

def make_x_batch(X, A, edge_index, pseudo):
    '''
    Erzeugt aus den Knotenmerkmalen einen für die Spline-CNNs geeigneten Batch
    :param X: Knotenmerkmale
    :param A: Adjzanzematrix
    :param edge_index: Adjazenzmatrix im COO-Format
    :param pseudo: Pseudokoordinaten bzw. Kantengewichte
    :return: Ein Batch-Objekt
    '''
    dlist = []
    x = X
    if x.shape[0] != batch_size: 
        x = x.reshape(batch_size, int(x.shape[0]/batch_size), x.shape[1])
    edge_index = edge_index
    pseudo = pseudo
    
    for batch in range(x.shape[0]):
        data=Data(x=x.select(0,batch), edge_index = edge_index, edge_attr = pseudo)
        dlist.append(data)
    
    x_batch = Batch.from_data_list(dlist)
    return x_batch

def count_values(A):
    values = 0
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] != 0:
                values += 1
    return values

def count_ones(A):
    ones = 0
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            if A[i][j] == 1:
                ones += 1
    return ones  


def ret_values(adj):
    '''
    Zählt alle Werte in adj die größer als 0 sind.
    :param adj: Adjazenzmatrix
    :return: Anzahl der Werte größer 0
    '''     
    vals = 0
    for i in range (adj.shape[0]):
        for j in range (adj.shape[1]):
            if adj[i][j] > 0.0:
                vals +=1 
    return vals

def intervalrange(X):
    '''
    Gibt den kleinsten und den größten Wert des übergebenen Arrays aus
    :param X: Array mit Knotenmerkmalen
    '''
    min = 50
    max = 0
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            for k in range (X.shape[2]):
                for l in range(X.shape[3]):
                    if (X[i][j][k][l] < min):
                        min = X[i][j][k][l]
                    if (X[i][j][k][l] > max):
                        max = X[i][j][k][l]
    print(f'Minimum:{min} und Maximum:{max}')


def make_batch_from_indices(indices, training_input, training_target, batch_size):
    input_datalist = []
    target_datalist = []
    for idx in indices:
        input_datalist.append(training_input[idx])
        target_datalist.append(training_target[idx])
    input_batch = Batch.from_data_list(input_datalist)
    target_batch = Batch.from_data_list(target_datalist)
    return input_batch, target_batch

def make_training_datalists(training_input, training_target, edge_index, edge_attr):
    inputlist = []
    targetlist = []
    x = torch.Tensor()
    x = x.new_empty(training_input.shape[1], training_input.shape[2])
    y = torch.Tensor()
    y = y.new_empty(training_target.shape[1], training_target.shape[2])
    for dc in range(training_input.shape[0]):
        for sensors in range(training_input.shape[1]):
            for ts in range(training_input.shape[2]):
                x[sensors][ts] = training_input[dc][sensors][ts][0]
        data = Data(x=x, edge_index = edge_index, edge_attr = edge_attr)
        inputlist.append(data)
        x = x.new_empty(training_input.shape[1], training_input.shape[2])
        print(f'Data {dc} von {training_input.shape[0]}')
    pickle.dump(inputlist, open('data_new\\pems-m\\training_input_datalist_12to6', 'wb'))
    for dc in range(training_target.shape[0]):
        for sensors in range(training_target.shape[1]):
            for ts in range(training_target.shape[2]):
                y[sensors][ts]= training_target[dc][sensors][ts]
        data = Data(x=y, edge_index = edge_index, edge_attr = edge_attr)
        targetlist.append(data)
        y = y.new_empty(training_target.shape[1], training_target.shape[2])
        print(f'Data {dc} von {training_target.shape[0]}')
    pickle.dump(targetlist, open('data_new\\pems-m\\training_target_datalist_12to6', 'wb'))
    return inputlist, targetlist

def make_val_datalists(val_input, val_target, edge_index, edge_attr):
    inputlist = []
    targetlist = []
    x = torch.Tensor()
    x = x.new_empty(val_input.shape[1], val_input.shape[2])
    y = torch.Tensor()
    y = y.new_empty(val_target.shape[1], val_target.shape[2])
    for dc in range(val_input.shape[0]):
        for sensors in range(val_input.shape[1]):
            for ts in range(val_input.shape[2]):
                x[sensors][ts] = val_input[dc][sensors][ts][0]
        data = Data(x=x, edge_index = edge_index, edge_attr = edge_attr)
        inputlist.append(data)
        x = x.new_empty(val_input.shape[1], val_input.shape[2])
        print(f'Data {dc} von {val_input.shape[0]}')
    pickle.dump(inputlist, open('data_new\\pems-m\\val_input_datalist_12to6', 'wb'))
    for dc in range(val_target.shape[0]):
        for sensors in range(val_target.shape[1]):
            for ts in range(val_target.shape[2]):
                y[sensors][ts]= val_target[dc][sensors][ts]
        data = Data(x=y, edge_index = edge_index, edge_attr = edge_attr)
        targetlist.append(data)
        y = y.new_empty(val_target.shape[1], val_target.shape[2])
        print(f'Data {dc} von {val_target.shape[0]}')
    pickle.dump(targetlist, open('data_new\\pems-m\\val_target_datalist_12to6', 'wb'))
    return inputlist, targetlist


import math
import torch
import time
import torch.nn as nn
import torch.nn.functional as F

import warnings

import torch
from torch.nn import Parameter
from torch_geometric.utils.repeat import repeat
from torch_geometric.data import Data
from torch_geometric.data import Batch

from utils_pems_12to3 import *

from torch_geometric.nn import (SplineConv)


class TimeBlock(nn.Module):
    """
    Zeitlicher Faltungsblock mit den Spline-CNNs
    """

    def __init__(self, in_channels, out_channels, kernel_size=3):
        """
        :param in_channels: Anzahl der Eingabemerkmale für jeden Knoten in jedem Zeitschritt.
        :param out_channels: Gewünschte Anzahl an Ausgabemerkmalen für jeden Knoten zu jedem Zeitschritt.
        :param kernel_size: Größe des zeitlichen Filters
        """
        super(TimeBlock, self).__init__()
        self.conv1 = SplineConv(in_channels, out_channels, dim = 2, kernel_size = 3)
        self.conv2 = SplineConv(in_channels, out_channels, dim = 2, kernel_size = 3)
        self.conv3 = SplineConv(in_channels, out_channels, dim = 2, kernel_size = 3)

    def forward(self, X, A_hat, edge_index, pseudo):
        """
        :param X: Eingabedaten
        :param A_hat: Normalisiserte Adjazenzmatrix
        :param edge_index: Adjazenzmatrix im COO-Format
        :param pseudo: Kantengewichte
        :return: Ausgabe der Form (batch_size, num_nodes,
        num_timesteps_out, num_features_out=out_channels)
        """
        if type(X) != type(Batch()):
            X = make_x_batch(X, A_hat, edge_index, pseudo)
        if X.x.dim() > 2:
           X.x = X.x.view(X.x.size(0), X.x.size(1) * X.x.size(2))
        conv1 = self.conv1(x = X.x, edge_index = X.edge_index, pseudo = X.edge_attr)
        conv2 = self.conv2(x = X.x, edge_index = X.edge_index, pseudo = X.edge_attr)
        out2 = torch.sigmoid(conv2)
        temp = conv1 + out2
        temp = temp.cuda()
        out = F.relu(temp + self.conv3(x = X.x, edge_index = X.edge_index, pseudo = X.edge_attr))
        # Convert back from NCHW to NHWC
        #out = out.permute(0, 2, 3, 1)
        return out

class SplineSTGCNBlock(nn.Module):
    """
    Räumlich-zeitlicher Faltungsblock (ST-Conv Block), welcher eine zeitliche Faltung,
    gefolgt von einer Graph-Convolution, gefolgt von einer zeitlichen Faltung für jeden
    Knoten ausführt.
    """

    def __init__(self, in_channels, spatial_channels, out_channels,
                 num_nodes):
        """
        :param in_channels: Anzahl von Eingabemerkmalen für jeden Knoten zu jedem Zeitschritt.
        :param spatial_channels: Anzahl von Ausgabemerkmalen der Graph-Convolution
        :param out_channels: Gewünschte Anzahl von Ausgabemerkmalen für jeden Knoten zu jedem Zeitschritt:
        :param num_nodes: Anzahl der Knoten des Graphen.
        """
        super(SplineSTGCNBlock, self).__init__()
        self.temporal1 = TimeBlock(in_channels=in_channels,
                                   out_channels=out_channels)
        self.Theta1 = nn.Parameter(torch.FloatTensor(out_channels,
                                                     spatial_channels))
        self.temporal2 = TimeBlock(in_channels=spatial_channels,
                                   out_channels=out_channels)
        self.batch_norm = nn.BatchNorm1d(num_nodes) #was 2D
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.Theta1.shape[1])
        self.Theta1.data.uniform_(-stdv, stdv)

    def forward(self, X, A_hat, edge_index, pseudo):
        """
        :param X: Eingabedaten der Form (batch_size, num_nodes, num_timesteps,
        num_features=in_channels).
        :param A_hat: Normalisiserte Adjazenzmatrix
        :param edge_index: Adjazenzmatrix im COO-Format
        :param pseudo: Kantengewichte
        :return: Ausgabe der Form (batch_size, num_nodes,
        num_timesteps_out, num_features=out_channels).
        """
        A_hat = A_hat.type(torch.FloatTensor)
        A_hat = A_hat.cuda()
        t = self.temporal1(X, A_hat, edge_index, pseudo)
        batch_size = int(t.shape[0] / 228)
        t = t.view(batch_size, 228, t.size(1))
        t = t.cuda()
        lfs = torch.einsum("ij,jkm->kim", [A_hat, t.permute(1, 0, 2)])
        t2 = F.relu(torch.matmul(lfs, self.Theta1))
        t3 = self.temporal2(t2, A_hat, edge_index, pseudo)
        t3 = t3.cuda()
        return t3


class SplineSTGCN(nn.Module):
    """
    Spline-STGCN Netzwerk
    """

    def __init__(self, num_nodes, num_features, num_timesteps_input,
                 num_timesteps_output):
        """
        :param num_nodes: Anzahl der Knoten des Graphen
        :param num_features: Anzahl von Merkmalen für jeden Knoten für jeden Zeitschritt
        :param num_timesteps_input: Anzahl von vergangenen Zeitschritten auf Basis dessen
        das Netzwerk lernt (Kontext)
        :param num_timesteps_output: Gewünschtes Zeitfenster für die Vorhersage
        """
        super(SplineSTGCN, self).__init__()
        self.block1 = SplineSTGCNBlock(in_channels=12, out_channels=48,
                                 spatial_channels=16, num_nodes=num_nodes)
        self.block2 = SplineSTGCNBlock(in_channels=48, out_channels=48,
                                 spatial_channels=16, num_nodes=num_nodes)
        self.last_temporal = TimeBlock(in_channels=48, out_channels=96)
        self.fully = nn.Linear((num_timesteps_input - 2 * 5) * 48,
                               num_timesteps_output)

    def forward(self, A_hat, X, edge_index, pseudo):
        """
        :param X: Eingabedaten der Form (batch_size, num_nodes, num_timesteps,
        num_features=in_channels).
        :param A_hat: Normalisierte Adjazenzmatrix
        :param edge_index: Adjazenzmatrix im COO-Format
        :param pseudo: Kantengewichte
        """
        out1 = self.block1(X, A_hat, edge_index, pseudo)
        out2 = self.block2(out1, A_hat, edge_index, pseudo)
        out3 = self.last_temporal(out2, A_hat, edge_index, pseudo)
        #out3_reshape = out3.reshape((out3.shape[0], out3.shape[1], -1))
        #out3_reshape = out3_reshape.reshape(10350, 48)
        out4 = self.fully(out3)
        out4 = out4.cuda()
        return out4
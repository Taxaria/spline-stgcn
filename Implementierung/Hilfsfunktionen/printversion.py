import torch
import matplotlib
import numpy
import pickle
import torch_geometric 
import scipy
import pandas

def versions():
    print(f'PyTorch: {torch.__version__}')
    print(f'Matplotlib: {matplotlib.__version__}')
    print(f'Numpy: {numpy.__version__}')
    print(f'Pickle: {pickle.format_version}')
    print(f'Pytorch Geometric: {torch_geometric.__version__}')
    print(f'SciPy: {scipy.__version__}')
    print(f'Pandas: {pandas.__version__}')

out = versions()
print(out)

# Spline-Spatio-Temporal Graph Convolutional Networks (Spline-STGCNs)

In diesem Repository befindet sich der Code für die Experimente auf dem METR-LA Datensatz und dem PeMSD7(M) Datensatz mit den Spline-STGCNs.

## Voraussetzungen

- matplotlib (3.0.3)
- numpy (1.16.2)
- torch (1.1.0)
- pickle (4.0)
- torch_geometric (1.2.0)
- scipy (1.2.1)
- pandas (0.24.2)
- python (3.7.2)

## Verwendete Modelle

- [Spline-CNNs aus der PyTorch Geometric Bibliothek](https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html#module-torch_geometric.nn.conv.message_passing)
- [Spatio-Temporal Graph Convolutional Networks](https://github.com/VeritasYin/STGCN_IJCAI-18)

## Lizenz

Das Projekt steht unter der MIT Lizenz.